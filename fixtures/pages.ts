import { test as baseTest } from '@playwright/test'
import { CheckoutPage } from '../pageObjects/checkoutPage'
import { LandingPage } from '../pageObjects/LandingPage'
import { MoisturizersPage } from '../pageObjects/MoisturizersPage'
import { SunscreensPage } from '../pageObjects/SunscreensPage'

/**
 * This extends base test to implement page fixtures. Fixtures help to
 * maintain a well organized page object model
*/
const test = baseTest.extend<{
  landingPage: LandingPage,
  moisturizersPage: MoisturizersPage,
  sunscreensPage: SunscreensPage,
  checkoutPage: CheckoutPage,
}>({
  landingPage: async ({ page }, use) => {
    await use(new LandingPage(page))
  },
  moisturizersPage: async ({ page }, use) => {
    await use(new MoisturizersPage(page))
  },
  sunscreensPage: async ({ page }, use) => {
    await use(new SunscreensPage(page))
  },
  checkoutPage: async ({ page }, use) => {
    await use(new CheckoutPage(page))
  },
})

export default test
export const expect = test.expect

