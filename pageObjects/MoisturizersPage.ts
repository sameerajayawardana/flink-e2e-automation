import { expect, Locator, Page } from '@playwright/test';
import { MOISTURIZER_PAGE } from '../routes/routes'
import { log } from '../util/logUtil';
import { extractAllProducts, Product } from '../util/productsUtil'

export class MoisturizersPage {
  readonly page: Page;
  readonly cartSpan: Locator;
  readonly productElementsSelector = 'div[class="container"] div[class="text-center col-4"]';

  constructor(page: Page) {
    this.page = page;
    this.cartSpan = page.locator('id=cart');
  }

  /**
   * This method will navigate browser to Moisturizers page
   *
   * @return {*}  {Promise<void>}
   * @memberof MoisturizersPage
   */
  async goto() : Promise<void> {
    await this.page.goto(MOISTURIZER_PAGE);
  }

  
  
  /**
   * This method will return product list in the page
   * 
   * @return {*}  {Promise<product[]>}
   * @memberof MoisturizersPage
   */
  async getAllProducts() : Promise<Product[]>{
    log('Extracting products from page');
    return extractAllProducts(this.page, this.productElementsSelector);
  }


  /**
   * Add given product to cart
   *
   * @param {Product} product
   * @return {*}  {Promise<void>}
   * @memberof MoisturizersPage
   */
  async addToCart(product: Product) : Promise<void> {
    log(`Adding '${product.name}' to cart`);

    await product.addButton.click();
  }


  /**
   * Method to open product cart
   *
   * @return {*}  {Promise<void>}
   * @memberof MoisturizersPage
   */
  async goToCart() : Promise<void> {
    log('Navigating to Cart');

    await this.cartSpan.click();
  }

  /**
   * This method return cart item count
   *
   * @return {*}  {Promise<number>}
   * @memberof MoisturizersPage
   */
   async getCartCount() : Promise<number> {
    log('Retrieving cart count');

    return parseInt((await this.cartSpan.textContent()).split(' item(s)')[0]);
  }
}