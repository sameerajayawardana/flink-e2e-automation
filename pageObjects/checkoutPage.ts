import { Locator, Page } from '@playwright/test';
import { CHECKOUT_PAGE, CONFIRMATION_PAGE } from '../routes/routes'
import { delay, log } from '../util/logUtil';
import { extractCartItems, Product } from '../util/productsUtil';
import { email, card_num, card_exp, card_cvc, zip } from '../data/user';

export class CheckoutPage {
  readonly page: Page;
  readonly totalPara: Locator;
  readonly payByCardButton: Locator;
  readonly confirmationHeader: Locator;
  readonly emailInput = 'id=email';
  readonly cardNumInput = 'id=card_number';
  readonly cardDateInput = 'id=cc-exp';
  readonly cardCVCInput = 'id=cc-csc';
  readonly zipInput = 'id=billing-zip';
  readonly payButton = 'id=submitButton';
  readonly tableRowSelector = 'table[class="table table-striped"] tbody tr';

  constructor(page: Page) {
    this.page = page;
    this.totalPara = page.locator('id=total');
    this.payByCardButton = page.locator('button[type="submit"]');
    this.confirmationHeader = page.locator('h2');
  }

  /**
   * This method will navigate browser to Sunscreens page
   *
   * @return {*}  {Promise<void>}
   * @memberof SunscreensPage
   */
  async goto() : Promise<void> {
    await this.page.goto(CHECKOUT_PAGE);
  }


  /**
   * This method will return all items in cart
   *
   * @return {*}  {Promise<product[]>}
   * @memberof SunscreensPage
   */
  async getCartItems() : Promise<Product[]>{
    log('Extracting products from cart');

    return extractCartItems(this.page, this.tableRowSelector);
  }


  /**
   * This method will submit payment
   *
   * @param {Product} product
   * @return {*}  {Promise<void>}
   * @memberof SunscreensPage
   */
  async payByCard() : Promise<void> {
    log(`Paying by card`);

    await this.payByCardButton.click();
    await delay(5000);

    const frame = await this.page.mainFrame().childFrames()[0];
    await (await frame.$(this.emailInput)).type(email, { delay: 50 });
    await (await frame.$(this.cardNumInput)).type(card_num, { delay: 50 });
    await (await frame.$(this.cardDateInput)).type(card_exp, { delay: 50 });
    await (await frame.$(this.cardCVCInput)).type(card_cvc, { delay: 50 });
    await (await frame.$(this.zipInput)).type(zip, { delay: 50 });
    await (await frame.$(this.payButton)).click();
    await this.page.waitForURL(CONFIRMATION_PAGE);
  }


  /**
   * This will return the payment confirm text
   *
   * @return {*}  {Promise<string>}
   * @memberof SunscreensPage
   */
  async getPaymentStatus() : Promise<string> {
    log('Retriving payment status');

    return this.confirmationHeader.textContent();
  }
}