import { Locator, Page } from '@playwright/test';
import { log } from '../util/logUtil';

export class LandingPage {
    readonly page: Page;
    readonly currentTempSpan: Locator;
    readonly buyMoisturizersButton: Locator;
    readonly buySunscreensButton: Locator;

    constructor(page: Page) {
        this.page = page;
        this.currentTempSpan = page.locator('[id="temperature"]');
        this.buyMoisturizersButton = page.locator('a[href="/moisturizer"] button');
        this.buySunscreensButton = page.locator('a[href="/sunscreen"] button');
    }

    /**
     * This method will navigate browser to landing page
     * @return {*}  {Promise<void>}
     * @memberof LandingPage
     */
    async goto(): Promise<void> {
        log('Navigating to Landing Page')
        await this.page.goto('/');
    }

    /**
     * This method will return the current temperature in landing page
     * @return {*}  {Promise<number>}
     * @memberof LandingPage
     */
    async getCurentTempreture(): Promise<number> {
        log('Getting current tempreture');
        return parseInt((await this.currentTempSpan.textContent()).split('℃')[0]);
    }

    /**
     * This method will click buyMoisturizersButton on landing page
     * @return {*}  {Promise<void>}
     * @memberof LandingPage
     */
    async gotoMoisturizersPage(): Promise<void> {
        log('Navigating to Moisturizers Page');
        await this.buyMoisturizersButton.click();
    }

    /**
     * This method will click buySunscreensButton on landing page
     * @return {*}  {Promise<void>}
     * @memberof LandingPage
     */
    async gotoSunscreensPage(): Promise<void> {
        log('Navigating to Sunscreens Page')
        await this.buySunscreensButton.click();
    }
}