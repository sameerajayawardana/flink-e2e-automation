import { PlaywrightTestConfig } from '@playwright/test';

const config: PlaywrightTestConfig = {
  use: {
    baseURL: (process.env.TEST_ENVIRONMENT=="prod"? process.env.PROD_BASE_URL : process.env.STAG_BASE_URL) || "https://weathershopper.pythonanywhere.com",
    headless: true,
    screenshot: "only-on-failure",
  },
  reporter: [ ['list'], ['allure-playwright'] ],

  projects: [
    {
      name: 'Chromium',
      use: {
        browserName: 'chromium',
      },
    },
    {
      name: 'Firefox',
      use: { browserName: 'firefox' },
    },
  ],
};
export default config;