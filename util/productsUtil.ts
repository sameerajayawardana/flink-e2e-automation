import { ElementHandle, Page } from "@playwright/test";
import * as _ from 'lodash';
import { log } from "./logUtil";

export type Product = {
    name: string,
    price: number,
    addButton?: ElementHandle<HTMLElement | SVGElement>
}


export const extractAllProducts = async (page: Page, elementSelector: string): Promise<Product[]> => {
    const products: Product[] = [];
    const elements = await page.$$(elementSelector);

    for (const element of elements) {
        products.push({
            name: await (await element.$(':nth-child(2)')).textContent(),
            price: parseInt((await (await element.$(':nth-child(3)')).textContent()).match(/\d+/)[0]),
            addButton: await element.$(':nth-child(4)')
        })
    };
    return products;
}


export const searchAndSortProducts = async (products: Product[], searchtext: string) : Promise<Product[]> => {
    log(`Searching and sorting products containing '${searchtext}'`);

    const filtered = products.filter(p => p.name.includes(searchtext));
    return _.sortBy(filtered, o => o.price);
}

export const extractCartItems = async (page: Page, elementSelector: string): Promise<Product[]> => {
    const products: Product[] = [];
    const elements = await page.$$(elementSelector);

    for (const element of elements) {
        products.push({
            name: await (await element.$(':nth-child(1)')).textContent(),
            price: parseInt((await (await element.$(':nth-child(2)')).textContent()).match(/\d+/)[0])
        })
    };
    return products;
}