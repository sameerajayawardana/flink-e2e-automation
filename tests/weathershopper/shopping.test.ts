import test, { expect } from '../../fixtures/pages';
import { browser, log, setBrowserForLogs } from '../../util/logUtil';
import { TEMP_THRESHOLD } from '../../data/common'
import { MoisturizersPage } from '../../pageObjects/MoisturizersPage';
import { SunscreensPage } from '../../pageObjects/SunscreensPage';
import { Product, searchAndSortProducts } from '../../util/productsUtil';

/**
 * TEST_SCENARIO
 * 1. Get current temperature from page
 * 2. Execute Moisturizer Checkout Test if temperature is below 19
 * 3. Execute Sunscreen Checkout Test if temperature is above 19
 */
test('primary flow - shopping based on temperature', async ({ landingPage, moisturizersPage, sunscreensPage, checkoutPage, browserName }) => {
  setBrowserForLogs(browserName);
  let selectedProducts: Product[] = [];


  await landingPage.goto();
  const temp = await landingPage.getCurentTempreture();
  console.log('Current temperature is ', temp, ' ℃');

  log('Asserting Buy moisturizers button enabled')
  expect(landingPage.buyMoisturizersButton).toBeEnabled();
  log('Asserting Buy sunscreen button enabled')
  expect(landingPage.buySunscreensButton).toBeEnabled();

  if (temp < TEMP_THRESHOLD) {
    await landingPage.gotoMoisturizersPage();
    selectedProducts = await moisturizerCheckoutTest(moisturizersPage);
  } else {
    await landingPage.gotoSunscreensPage();
    selectedProducts = await sunscreenCheckoutTest(sunscreensPage);
  }

  const checkoutItems = await checkoutPage.getCartItems();
  console.log('Cart Items: ', checkoutItems);

  log('Asserting cart items added correctly')
  expect(checkoutItems[0].name).toEqual(selectedProducts[0].name);
  expect(checkoutItems[0].price).toEqual(selectedProducts[0].price);
  expect(checkoutItems[1].name).toEqual(selectedProducts[1].name);
  expect(checkoutItems[1].price).toEqual(selectedProducts[1].price);

  await checkoutPage.payByCard();

  log('Asserting payment status')
  expect(await checkoutPage.getPaymentStatus()).toEqual('PAYMENT SUCCESS');
});

/**
 * TEST_SCENARIO
 * 1. Add least expensive mositurizer that contains Aloe
 * 2. Add least expensive mositurizer that contains Almond
 * 3. Click on cart
 */
const moisturizerCheckoutTest = async (moisturizersPage: MoisturizersPage) : Promise<Product[]> => {
  log('Executing Moisturizer Checkout Test');
  const selectedProducts: Product[] = [];

  const productsList = await moisturizersPage.getAllProducts();

  // Selecting least expensive Aloe product
  const aloeList = await searchAndSortProducts(productsList, 'Aloe');
  await moisturizersPage.addToCart(aloeList[0]);

  // Selecting least expensive Almond product
  const almondList = await searchAndSortProducts(productsList, 'Almond');
  await moisturizersPage.addToCart(almondList[0]);

  log('Asserting cart item count');
  expect(await moisturizersPage.getCartCount()).toEqual(2);

  // Navigate to cart
  await moisturizersPage.goToCart();

  selectedProducts.push(aloeList[0]);
  selectedProducts.push(almondList[0]);
  return selectedProducts;
}

/**
 * TEST_SCENARIO
 * 1. Add least expensive sunscreen that is SPF-50 to cart
 * 2. Add least expensive sunscreen that is SPF-30 to cart
 * 3. Click on cart
 */
const sunscreenCheckoutTest = async (sunscreensPage: SunscreensPage) : Promise<Product[]> => {
  log('Executing Sunscreen Checkout Test');
  const selectedProducts: Product[] = [];

  const productsList = await sunscreensPage.getAllProducts();

  // Selecting least expensive PSF-50 product
  const spf50List = await searchAndSortProducts(productsList, 'SPF-50');
  await sunscreensPage.addToCart(spf50List[0]);

  // Selecting least expensive PSF-30 product
  const spf30List = await searchAndSortProducts(productsList, 'SPF-30');
  await sunscreensPage.addToCart(spf30List[0]);

  log('Asserting cart item count');
  expect(await sunscreensPage.getCartCount()).toEqual(2);

  // Navigate to cart
  await sunscreensPage.goToCart();

  selectedProducts.push(spf50List[0]);
  selectedProducts.push(spf30List[0]);
  return selectedProducts;
}