export const MOISTURIZER_PAGE = "/moisturizer"
export const SUNSCREEN_PAGE = "/sunscreen"
export const CHECKOUT_PAGE = "/cart"
export const CONFIRMATION_PAGE = "/confirmation"
